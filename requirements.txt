# Production
django==2.1
uwsgi==2.0.17.1
djangorestframework==3.8.2
coreapi==2.3.3
psycopg2-binary==2.7.5
drf-yasg==1.9.2
djangorestframework_simplejwt==3.2.3
drf-nested-routers==0.90.2

# Develop
ipython
pylint
pylint_django
model_mommy
pyenchant