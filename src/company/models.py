"""Company Model"""

from django.db import models
from django.utils.translation import gettext_lazy as _


class CorporateIdentity(models.Model):
    """Model definition for CorporateIdentity."""
    name = models.CharField(_('name'), unique=True, max_length=48)
    rif = models.CharField(_('rif'), unique=True, null=True, max_length=48)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('company')
        verbose_name_plural = _('companies')

    def __str__(self):
        """Unicode representation of CorporateIdentity."""
        return self.name


class Company(models.Model):
    """Model definition for CorporateIdentity."""
    corporate_identity = models.OneToOneField(
        CorporateIdentity, on_delete=models.CASCADE
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('customer company')
        verbose_name_plural = _('customer companies')


class Business(models.Model):
    """Model definition for CorporateIdentity."""
    corporate_identity = models.OneToOneField(
        CorporateIdentity, on_delete=models.CASCADE
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('vendor company')
        verbose_name_plural = _('vendor companies')
