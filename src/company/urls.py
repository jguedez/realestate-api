"""Routes for company app"""

from extensions.routers import Router

from company.views import (
    CorporateIdentityViewSet, CompanyViewSet, BusinessViewSet
)

router = Router()  # pylint: disable=C0103

router.register(
    'corporate-identity', CorporateIdentityViewSet, 'corporate-identity'
)
router.register('companies', CompanyViewSet, 'company')
router.register('businesses', BusinessViewSet, 'business')

urlpatterns = router.urls  # pylint: disable=C0103
