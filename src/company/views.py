"""Company viewsets"""

from rest_framework.viewsets import ModelViewSet

from company.models import CorporateIdentity, Company, Business
from company.serializers import (
    CorporateIdentitySerializer,
    CompanySerializer,
    BusinessSerializer
)


class CorporateIdentityViewSet(ModelViewSet):
    """Viewset for corporate identity"""
    queryset = CorporateIdentity.objects.all()
    serializer_class = CorporateIdentitySerializer


class CompanyViewSet(ModelViewSet):
    """Viewset for corporate identity"""
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class BusinessViewSet(ModelViewSet):
    """Viewset for corporate identity"""
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
