"""Serializers for company"""

from extensions.serializers import ModelSerializer
from company.models import Company, Business, CorporateIdentity


class CorporateIdentitySerializer(ModelSerializer):
    """Corporate Identity Serializer"""

    class Meta:  # pylint: disable=C0111
        fields = '__all__'
        model = CorporateIdentity


class CompanySerializer(ModelSerializer):
    """Company Serializer"""

    class Meta:  # pylint: disable=C0111
        fields = '__all__'
        model = Company


class BusinessSerializer(ModelSerializer):
    """Business Serializer"""

    class Meta:  # pylint: disable=C0111
        fields = '__all__'
        model = Business
