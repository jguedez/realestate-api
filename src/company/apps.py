"""Application module"""
from django.apps import AppConfig


class CompanyConfig(AppConfig):
    """Application configuration"""
    name = 'company'
