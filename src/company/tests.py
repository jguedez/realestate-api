"""Company tests"""

from model_mommy import mommy
from rest_framework.test import APITestCase
from extensions.tests import (ListTestMixin, CreateTestMixin, DestroyTestMixin)

from company.models import CorporateIdentity, Company, Business


class ListCorporateIdentityTest(ListTestMixin, APITestCase):
    """List all basic info for registered companies"""
    require_authentication = True
    base_name = 'corporate-identity'
    model = CorporateIdentity


class CreateCorporateIdentityTest(CreateTestMixin, APITestCase):
    """Create basic info and register a new company"""
    require_authentication = True
    base_name = 'corporate-identity'
    model = CorporateIdentity

    body_example = {'name': 'Facebook', 'rif': '1212312412'}
    require_fields = {
        'name',
    }
    unique_fields = {'name', 'rif'}
    response_example = {'id', 'name', 'rif'}


class DestroyCorporateIdentityTest(DestroyTestMixin, APITestCase):
    """Delete basic info and delete a company"""
    require_authentication = True
    base_name = 'corporate-identity'
    model = CorporateIdentity


class ListCompanyTest(ListTestMixin, APITestCase):
    """List all basic info for registered companies"""
    require_authentication = True
    base_name = 'company'
    model = Company


class CreateCompanyTest(CreateTestMixin, APITestCase):
    """Create basic info and register a new company"""
    require_authentication = True
    base_name = 'company'
    model = Company

    @classmethod
    def setUpTestData(cls):
        cls.body_example = {
            'corporate_identity': mommy.make('company.CorporateIdentity').id
        }

    require_fields = {
        'corporate_identity',
    }
    unique_fields = {'corporate_identity'}
    response_example = {'id', 'corporate_identity'}


class DestroyCompanyTest(DestroyTestMixin, APITestCase):
    """Delete basic info and delete a company"""
    require_authentication = True
    base_name = 'company'
    model = Company


class ListBusinessTest(ListTestMixin, APITestCase):
    """List all basic info for registered companies"""
    require_authentication = True
    base_name = 'business'
    model = Business


class CreateBusinessTest(CreateTestMixin, APITestCase):
    """Create basic info and register a new company"""
    require_authentication = True
    base_name = 'business'
    model = Business

    @classmethod
    def setUpTestData(cls):
        cls.body_example = {
            'corporate_identity': mommy.make('company.CorporateIdentity').id
        }

    require_fields = {
        'corporate_identity',
    }
    unique_fields = {'corporate_identity'}
    response_example = {'id', 'corporate_identity'}


class DestroyBusinessTest(DestroyTestMixin, APITestCase):
    """Delete basic info and delete a company"""
    require_authentication = True
    base_name = 'business'
    model = Business
