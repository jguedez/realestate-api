"""Application module"""
from django.apps import AppConfig


class CaseConfig(AppConfig):
    """Application configuration"""
    name = 'case'
