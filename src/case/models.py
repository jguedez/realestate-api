"""Cases Model"""
from django.utils.translation import gettext_lazy as _
from django.db import models
from common.models import LoggingMixin


class TypeCase(models.Model):
    """Model definition for TypeCase."""
    name = models.CharField(_('name'), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('TypeCase')
        verbose_name_plural = ('TypeCases')

    def __str__(self):
        """Unicode representation of TypeCase."""
        return self.name


class Priority(models.Model):
    """Model definition for Priority."""
    name = models.CharField(_('name'), unique=True, max_length=64)
    importance = models.IntegerField(_('importante'))

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Priority')
        verbose_name_plural = _('Priorities')

    def __str__(self):
        """Unicode representation of Priority."""
        return f'{self.importance}: {self.name}'


class StatusCase(models.Model):
    """Model definition for StatusCase."""
    name = models.CharField(_('name'), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('StatusCase')
        verbose_name_plural = _('StatusCases')

    def __str__(self):
        """Unicode representation of StatusCase."""
        return self.name


class OriginCase(models.Model):
    """Model definition for OriginCase."""
    name = models.CharField(_('name'), max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('OriginCase')
        verbose_name_plural = _('OriginCases')

    def __str__(self):
        """Unicode representation of OriginCase."""
        return self.name


class Case(LoggingMixin):
    """Model definition for Case."""
    subject = models.CharField(_('subject'), max_length=128)
    no_ticket = models.CharField(_('no ticket'), unique=True, max_length=50)
    motive = models.TextField(_('motive case'))
    description = models.TextField(_('description'))
    solution = models.CharField(_('solution'), max_length=128)
    priority = models.ForeignKey(Priority, on_delete=models.PROTECT)
    type_case = models.ForeignKey(TypeCase, on_delete=models.PROTECT)
    status = models.ForeignKey(StatusCase, on_delete=models.PROTECT)
    origin = models.ForeignKey(OriginCase, on_delete=models.PROTECT)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Case')
        verbose_name_plural = _('Cases')

    def __str__(self):
        """Unicode representation of Case."""
        return self.subject


class Comment(LoggingMixin):
    """Model definition for Comment."""
    COMMENT_TYPE_CHOICES = [('Inter', 'Interno'), ('Ext', 'Externo')]
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    content = models.CharField(_("content"), max_length=225)
    types = models.CharField(max_length=10, choices=COMMENT_TYPE_CHOICES)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __str__(self):
        """Unicode representation of Comment."""
        return self.content[:30]
