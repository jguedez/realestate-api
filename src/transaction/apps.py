"""Application module"""
from django.apps import AppConfig


class TransactionConfig(AppConfig):
    """Application configuration"""
    name = 'transaction'
