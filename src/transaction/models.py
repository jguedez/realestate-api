"""Transaction models"""

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _

from common.models import Address
from product.models import Product, Price, Currency
from person.models import Agent
from common.models import LoggingMixin


class Stage(models.Model):
    """ Stage Module """
    name = models.CharField(_('name'), unique=True, max_length=128)
    probability = models.DecimalField(
        _('probability'), max_digits=10, decimal_places=2
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Stage')
        verbose_name_plural = _('Stages')

    def __str__(self):
        return f'{self.name} {self.probability}'


class Transaction(LoggingMixin):
    """ Transaction Module """
    subject = models.CharField(_('subject'), max_length=128)
    description = models.TextField(_('description'))
    closing_date = models.DateField(_('closing date'))
    extra_field = JSONField('extra fields')
    comission_buyer = models.DecimalField(
        _('comission buyer'), max_digits=3, decimal_places=1
    )
    comission_seller = models.DecimalField(
        _('comission seller'), max_digits=3, decimal_places=1
    )
    royalty_buyer = models.DecimalField(
        _('royalty buyer'), max_digits=3, decimal_places=1
    )
    royalty_seller = models.DecimalField(
        _('royalty seller'), max_digits=3, decimal_places=1
    )
    stage = models.ForeignKey(Stage, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.ForeignKey(Price, on_delete=models.PROTECT)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')

    def __str__(self):
        return self.subject


class Order(LoggingMixin):
    """Model definition for Sales/Purchase Orders."""
    subject = models.CharField(_('subject'), max_length=128)
    number = models.CharField(_('number'), max_length=64, unique=True)
    requisition_no = models.IntegerField(_('requisition number'))
    due_date = models.DateField(_('due date'))
    excise_dute = models.DecimalField(
        _('excise dute'), max_digits=10, decimal_places=2
    )
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    term_conditions = models.TextField(_('terms and condition'))
    description = models.TextField(_('description'))

    class Meta:  # pylint: disable=C0111
        abstract = True


class Invoice(Order):
    """ Invoice Module """

    class Meta:  # pylint: disable=C0111
        verbose_name = _("invoice")
        verbose_name_plural = _("invoices")

    def __str__(self):
        return self.name


class PurchaseOrder(Order):
    """ Purchase Orders Module """
    percent_comission = models.FloatField(_('percent comission'))
    agent_advisor = models.ForeignKey(Agent, on_delete=models.CASCADE)
    billing_address = models.OneToOneField(
        Address, on_delete=models.PROTECT, related_name='purchase_billed'
    )
    shipping_address = models.OneToOneField(
        Address, on_delete=models.PROTECT, related_name='purchase_shipped'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Purchase Order')
        verbose_name_plural = _('Purchase Orders')


class SalesOrder(Order):
    """ Sales Orders Module """
    billing_address = models.OneToOneField(
        Address, on_delete=models.PROTECT, related_name='sales_billed'
    )
    shipping_address = models.OneToOneField(
        Address, on_delete=models.PROTECT, related_name='sales_shipped'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Sales Order')
        verbose_name_plural = _('Sales Orders')


class PaymentMethod(models.Model):
    """ Payment Methods Module """
    name = models.CharField(_('name'), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Payment Method')
        verbose_name_plural = _('Payments Methods')

    def __str__(self):
        return self.name


class Bank(models.Model):
    """ Bank Module """
    name = models.CharField(_('name'), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Bank')
        verbose_name_plural = _('Banks')

    def __str__(self):
        return self.name


class Payments(LoggingMixin):
    """Model definition for Payments."""
    ref_code = models.CharField(_('ref code'), max_length=64)
    amount = models.DecimalField(_('amount'), max_digits=10, decimal_places=2)
    date = models.DateField(_('date'))
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    method = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)

    class Meta:  # pylint: disable=C0111
        abstract = True


class SOPayment(Payments):
    """Model definition for Sales Orders Payments."""
    conciliation = models.BooleanField(_('concilitation'))
    sale = models.ForeignKey(SalesOrder, on_delete=models.CASCADE)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Sales Order Payment')
        verbose_name_plural = _('Sales Orders Payments')


class POPayment(Payments):
    """Model definition for Purchase Orders Payments."""
    purchase = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Purchase Order Payment')
        verbose_name_plural = _('Purchase Orders Payments')
