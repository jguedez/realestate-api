"""Activity Model"""
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _
from django.db import models

from person.models import Agent
from common.models import LoggingMixin


class ActivityType(models.Model):
    """Type Activities of Users """
    name = models.CharField(_('subjets'), max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Activity')
        verbose_name_plural = _('Activities')

    def __str__(self):
        return self.name


class Activity(LoggingMixin):
    """Activities of Users """
    subject = models.CharField(_('subjets'), max_length=64)
    value = JSONField("values")
    comment = models.CharField(
        _("comments"), max_length=50, blank=True, null=True
    )
    activity_type = models.ForeignKey(
        ActivityType, on_delete=models.CASCADE, null=True
    )
    invited_agents = models.ManyToManyField(
        Agent, related_name='invited_activities'
    )
    agent = models.ForeignKey(
        Agent, on_delete=models.PROTECT, related_name='activities'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Activity')
        verbose_name_plural = _('Activities')

    def __str__(self):
        return self.subject
