"""Application module"""
from django.apps import AppConfig


class ActivityConfig(AppConfig):
    """Application configuration"""
    name = 'activity'
