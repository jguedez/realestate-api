from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework_simplejwt.tokens import AccessToken

from rest_framework import status
from model_mommy import mommy


class BaseTest:
    require_authentication = False
    base_name = None
    model = None
    action = None

    def request(self, args=None, body=None):
        suffix = 'list' if self.action in ['list', 'create'] else 'detail'
        methods = {
            'create': 'post',
            'list': 'get',
            'update': 'patch',
            'destroy': 'delete',
            'retrieve': 'get'
        }
        url = reverse(f'{self.base_name}-{suffix}', args=args)
        method = getattr(self.client, methods[self.action])
        self.response = method(url, data=body, format='json')

    def authenticate(self):
        if self.require_authentication:
            Account = get_user_model()
            account = mommy.make(Account)
            token = AccessToken.for_user(account)
            self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(token))

    def valid_request(self):
        raise NotImplementedError

    def test_unauthenticate_status_code(self):
        self.valid_request()
        status_code = self.response.status_code
        if self.require_authentication:
            self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)
        else:
            self.assertNotEqual(status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unauthenticate_response(self):
        self.valid_request()
        data = self.response.data
        if self.require_authentication:
            self.assertIn('detail', data)
        else:
            self.assertNotIn('detail', data)


class DestroyTestMixin(BaseTest):
    action = 'destroy'

    @classmethod
    def setUpTestData(cls):
        mommy.make(cls.model, 50)

    def valid_request(self):
        first_id = self.model.objects.first().id
        self.request(args=[first_id])

    def not_existed_request(self):
        not_existed_id = self.model.objects.last().id + 1
        self.request(args=[not_existed_id])

    # STATUS CODE
    def test_delete_status_code(self):
        self.authenticate()
        self.valid_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_204_NO_CONTENT)

    def test_not_existed_status_code(self):
        self.authenticate()
        self.not_existed_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_404_NOT_FOUND)

    # DATABASE STATE
    def test_delete_of_database(self):
        before_count = self.model.objects.count()
        self.authenticate()
        self.valid_request()
        after_count = self.model.objects.count()
        self.assertEqual(before_count - 1, after_count)


class CreateTestMixin(BaseTest):
    action = 'create'

    body_example = dict()
    require_fields = set()
    unique_fields = set()
    response_example = set()

    def valid_request(self):
        self.request(body=self.body_example)

    def missing_request(self):
        self.request(body={})

    def existing_request(self):
        self.valid_request()
        self.valid_request()

    # STATUS CODE
    def test_valid_status_code(self):
        self.authenticate()
        self.valid_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_201_CREATED)

    def test_missing_require_status_code(self):
        self.authenticate()
        self.missing_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_400_BAD_REQUEST)

    def test_unique_fields_status_code(self):
        self.authenticate()
        self.existing_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_400_BAD_REQUEST)

    # RESPONSE
    def test_valid_response(self):
        self.authenticate()
        self.valid_request()
        data = self.response.data
        self.assertSetEqual(set(self.response_example), set(data))

    def test_missing_require_response(self):
        self.authenticate()
        self.missing_request()
        data = self.response.data
        self.assertSetEqual(set(self.require_fields), set(data))

    def test_unique_fields_response(self):
        self.authenticate()
        self.existing_request()
        data = self.response.data
        self.assertSetEqual(set(self.unique_fields), set(data))

    # DATABASE STATE
    def test_valid_database_state(self):
        before_count = self.model.objects.count()
        self.authenticate()
        self.valid_request()
        after_count = self.model.objects.count()
        self.assertEqual(before_count + 1, after_count)

    def test_missing_require_database_state(self):
        before_count = self.model.objects.count()
        self.authenticate()
        self.missing_request()
        after_count = self.model.objects.count()
        self.assertEqual(before_count, after_count)

    def test_unique_fields_database_state(self):
        self.authenticate()
        self.valid_request()
        before_count = self.model.objects.count()
        self.valid_request()
        after_count = self.model.objects.count()
        self.assertEqual(before_count, after_count)

    def test_unauthenticate_database_state(self):
        before_count = self.model.objects.count()
        self.valid_request()
        after_count = self.model.objects.count()
        self.assertEqual(before_count, after_count)


class ListTestMixin(BaseTest):
    action = 'list'

    max_results = 20
    is_paginated = True

    @classmethod
    def setUpTestData(cls):
        mommy.make(cls.model, 50)

    def get_results(self, data):
        if self.is_paginated:
            return data['results']
        return data

    def valid_request(self):
        self.request()

    # STATUS CODE
    def test_valid_status_code(self):
        self.authenticate()
        self.valid_request()
        status_code = self.response.status_code
        self.assertEqual(status_code, status.HTTP_200_OK)

    # RESPONSE
    def test_count_response(self):
        self.authenticate()
        self.valid_request()
        data = self.get_results(self.response.data)
        self.assertGreater(len(data), 0)

    def test_max_count_response(self):
        self.authenticate()
        self.valid_request()
        data = self.get_results(self.response.data)
        self.assertLessEqual(len(data), self.max_results)

    def test_paginated_response(self):
        paginated_keys = {'count', 'next', 'previous', 'results'}
        self.authenticate()
        self.valid_request()
        if self.is_paginated:
            data = set(self.response.data)
            self.assertSetEqual(paginated_keys, data)
        else:
            self.assertNotEqual(paginated_keys, self.response.data)


class NotAllowBaseTest:
    def authenticate(self):
        Account = get_user_model()
        account = mommy.make(Account)
        token = AccessToken.for_user(account)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + str(token))


class ListNotAllowMixin(NotAllowBaseTest):
    def test_list_not_allow(self):
        self.authenticate()
        url = reverse(self.base_name + '-list')
        response = self.client.get(url)
        status_code = response.status_code
        self.assertEqual(status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class CreateNotAllowMixin(NotAllowBaseTest):
    def test_create_not_allow(self):
        self.authenticate()
        url = reverse(self.base_name + '-list')
        response = self.client.post(url)
        status_code = response.status_code
        self.assertEqual(status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class RetrieveNotAllowMixin(NotAllowBaseTest):
    def test_retrieve_not_allow(self):
        self.authenticate()
        url = reverse(self.base_name + '-detail', args=[1])
        response = self.client.get(url)
        status_code = response.status_code
        self.assertEqual(status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class UpdateNotAllowMixin(NotAllowBaseTest):
    def test_update_not_allow(self):
        self.authenticate()
        url = reverse(self.base_name + '-detail', args=[1])
        response = self.client.patch(url)
        status_code = response.status_code
        self.assertEqual(status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class DestroyNotAllowMixin(NotAllowBaseTest):
    def test_destroy_not_allow(self):
        self.authenticate()
        url = reverse(self.base_name + '-detail', args=[1])
        response = self.client.delete(url)
        status_code = response.status_code
        self.assertEqual(status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class ReadOnlyAllowMixin(
        DestroyNotAllowMixin,
        UpdateNotAllowMixin,
        CreateNotAllowMixin):
    pass
