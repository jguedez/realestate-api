import time

from unittest.runner import TextTestResult
from django.test.runner import DiscoverRunner


class TimeLoggingTestResult(TextTestResult):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_timings = []

    def startTest(self, test):
        self._test_started_at = time.time()
        super().startTest(test)

    def addSuccess(self, test):
        elapsed = time.time() - self._test_started_at
        self.test_timings.append((str(test), elapsed))
        super().addSuccess(test)

    def tests_timings(self):
        return self.test_timings


class TimeLoggingTestRunner(DiscoverRunner):
    def get_resultclass(self):
        return TimeLoggingTestResult

    def suite_result(self, suite, result, **kwargs):
        timings = [x for x in result.tests_timings() if x[1] > 0.5]
        if timings:
            print('-' * 70)
            print('\nTests that take more than 0.5 seconds')
            for name, elapsed in timings:
                print("({:.03}s) {}".format(elapsed, name))
            print('-' * 70)
        return super().suite_result(suite, result, **kwargs)
