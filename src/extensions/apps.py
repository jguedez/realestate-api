"""Application module"""
from django.apps import AppConfig


class ExtensionsConfig(AppConfig):
    """Application configuration"""
    name = 'extensions'
