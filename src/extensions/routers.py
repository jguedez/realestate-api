"""Router file"""

import re
from collections import namedtuple
from typing import List
from rest_framework_nested import routers
from rest_framework.routers import SimpleRouter, Route, DynamicRoute

NestedRoute = namedtuple('NestedRoute', ['prefix', 'viewset', 'base_name'])


class Router(SimpleRouter):
    """Generic router without put"""
    routes = [
        # List route.
        Route(
            url=r'^{prefix}{trailing_slash}$',
            mapping={
                'get': 'list',
                'post': 'create'
            },
            name='{basename}-list',
            detail=False,
            initkwargs={'suffix': 'List'}
        ),
        # Dynamically generated list routes. Generated using
        # @action(detail=False) decorator on methods of the viewset.
        DynamicRoute(
            url=r'^{prefix}/{url_path}{trailing_slash}$',
            name='{basename}-{url_name}',
            detail=False,
            initkwargs={}
        ),
        # Detail route.
        Route(
            url=r'^{prefix}/{lookup}{trailing_slash}$',
            mapping={
                'get': 'retrieve',
                'patch': 'partial_update',
                'delete': 'destroy'
            },
            name='{basename}-detail',
            detail=True,
            initkwargs={'suffix': 'Instance'}
        ),
        # Dynamically generated detail routes. Generated using
        # @action(detail=True) decorator on methods of the viewset.
        DynamicRoute(
            url=r'^{prefix}/{lookup}/{url_path}{trailing_slash}$',
            name='{basename}-{url_name}',
            detail=True,
            initkwargs={}
        ),
    ]

    def __init__(self, *args, **kwargs):
        self._urls = None
        super().__init__(*args, **kwargs)
        self.nested_routes = []

    def register(  # pylint: disable=W0221
            self,
            prefix,
            viewset,
            base_name=None,
            nested: List[NestedRoute] = None,
            lookup=None):
        super().register(prefix, viewset, base_name)
        if lookup is None:
            lookup = re.sub(r'(ies|s|es)$', '', prefix)

        if nested:
            for nest in nested:
                route = routers.NestedSimpleRouter(self, prefix, lookup=lookup)
                route.register(*nest)
                self.nested_routes.extend(route.urls)

    @property
    def urls(self):
        if not self._urls:
            self._urls = self.get_urls()
        return self._urls + self.nested_routes
