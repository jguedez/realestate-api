"""Extensions serializers"""

from rest_framework import serializers


class ModelSerializer(serializers.ModelSerializer):
    """ModelSerializer for flatten"""

    class Meta:  # pylint: disable=C0111
        flatten = []

    def get_fields(self):
        fields = super().get_fields()
        if not hasattr(self.Meta, 'flatten'):
            return fields

        flattens = self.Meta.flatten

        for field_name, field in fields.copy().items():
            if field_name in flattens:
                del fields[field_name]
                nested_fields = field.fields
                for nest_field_name, nest_field in nested_fields.items():
                    source = nest_field.source or nest_field_name
                    nest_field.source = f'{field_name}.{source}'
                    fields[nest_field_name] = nest_field

        return fields
