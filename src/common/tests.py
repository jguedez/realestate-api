"""Test common"""

from rest_framework import status
from rest_framework.test import APITestCase
from model_mommy import mommy

from extensions.tests import (
    ListTestMixin, DestroyNotAllowMixin, CreateNotAllowMixin, BaseTest
)

from common.models import Country


class ListCountries(ListTestMixin, APITestCase):
    require_authentication = False
    base_name = 'country'
    model = Country
    is_paginated = False
    max_results = 400


class UpdateCountries(BaseTest, APITestCase):
    action = 'update'

    require_authentication = True
    base_name = 'country'
    model = Country

    def setUp(self):
        self.country = mommy.make(Country, active=False)

    def valid_request(self):
        self.request(args=[self.country.id], body={'active': True})

    def test_modify_active_status_code(self):
        self.authenticate()
        self.valid_request()
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_modify_active_database(self):
        self.authenticate()
        self.valid_request()
        self.country.refresh_from_db()
        self.assertTrue(self.country.active)


class DestroyCreateNotAllowTest(
        DestroyNotAllowMixin,
        CreateNotAllowMixin,
        APITestCase):
    base_name = 'country'
