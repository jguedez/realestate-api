"""

"""
from extensions.routers import Router

from common.views import CountryViewSet, CommercialOfficeViewSet

router = Router()
router.register('countries', CountryViewSet, base_name='country')
router.register(
    'commercial-office', CommercialOfficeViewSet, base_name='commercialoffice'
)

urlpatterns = router.urls
