from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _
from django.contrib.postgres.indexes import BrinIndex


class LoggingMixin(models.Model):
    """Model definition for LoggingMixin."""
    created_on = models.DateTimeField(_('created on'), auto_now_add=True)
    updated_on = models.DateTimeField(_('updated on'), auto_now=True)

    class Meta:  # pylint: disable=C0111
        abstract = True
        indexes = (BrinIndex(fields=['created_on']), )


class Country(models.Model):
    name = models.CharField(_('names'), unique=True, max_length=50)
    active = models.BooleanField(_('active'))

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.name


class State(models.Model):
    """ State of countries """
    name = models.CharField(_('names'), max_length=64)

    country = models.ForeignKey(
        Country, on_delete=models.PROTECT, related_name='states'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('State')
        verbose_name_plural = _('States')

    def __str__(self):
        return self.name


class City(models.Model):
    """ State of countries """
    name = models.CharField(_('names'), max_length=64)

    state = models.ForeignKey(
        State, on_delete=models.PROTECT, related_name='cities'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('City')
        verbose_name_plural = _('Cities')

    def __str__(self):
        return self.name


class Address(models.Model):
    """ Address of countries """
    locality = models.CharField(_('localities'), max_length=128, blank=True)
    street = models.CharField(_('streets'), max_length=128, blank=True)
    postal_code = models.CharField(_('postal code'), max_length=40, blank=True)
    lat = models.CharField(_('lats'), max_length=128)
    long = models.CharField(_('longs'), max_length=128)

    city = models.ForeignKey(
        City, on_delete=models.PROTECT, related_name='cities'
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    def __str__(self):
        return f'{self.lat}, {self.long}'


class Setting(models.Model):
    """Model definition for Settings."""
    name = models.CharField(_('names'), unique=True, max_length=64)
    value = JSONField('values')

    class Meta:  # pylint: disable=C0111
        verbose_name = _('Setting')
        verbose_name_plural = _('Settings')

    def __str__(self):
        return self.name


class Source(models.Model):
    """Model definition for Source."""
    name = models.CharField(_('name'), unique=True, max_length=32)

    class Meta:  # pylint: disable=C0111
        verbose_name = _("source")
        verbose_name_plural = _("sources")

    def __str__(self):
        """Unicode representation of Source."""
        return self.name


class CommercialOffice(models.Model):
    """Commercial Office Model"""
    name = models.CharField(_('name'), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _("commercial office")
        verbose_name_plural = _("commercial offices")

    def __str__(self):
        return self.name
