"""Views for common
"""

from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from common.models import Country, CommercialOffice
from common.serializers import (
    CountrySerializer, CountryRetrieveSerializer, CommercialOfficeSerializer
)


class CountryViewSet(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    queryset = Country.objects.all()
    pagination_class = None
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CountryRetrieveSerializer
        return CountrySerializer


class CommercialOfficeViewSet(viewsets.ReadOnlyModelViewSet):
    """View of CommercialOffice Only retrieve value"""
    queryset = CommercialOffice.objects.all()
    serializer_class = CommercialOfficeSerializer
