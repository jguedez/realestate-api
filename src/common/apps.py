"""Application module"""
from django.apps import AppConfig


class CommonConfig(AppConfig):
    """Application configuration"""
    name = 'common'
