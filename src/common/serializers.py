"""Serializers of common"""

from rest_framework.serializers import ModelSerializer
from common.models import Country, State, City, Address
from common.models import CommercialOffice


class AddressSerializer(ModelSerializer):
    class Meta:  # pylint: disable=C0111
        model = Address
        exclude = ('id', )


class CountrySerializer(ModelSerializer):
    class Meta:  # pylint: disable=C0111
        model = Country
        fields = '__all__'


class CitySerializer(ModelSerializer):
    class Meta:  # pylint: disable=C0111
        model = City
        fields = ('id', 'name')


class StateSerializer(ModelSerializer):
    cities = CitySerializer(read_only=True, many=True)

    class Meta:  # pylint: disable=C0111
        model = State
        fields = ('id', 'name', 'cities')


class CountryRetrieveSerializer(ModelSerializer):
    states = StateSerializer(read_only=True, many=True)

    class Meta:  # pylint: disable=C0111
        model = Country
        fields = ('id', 'name', 'states')


class CommercialOfficeSerializer(ModelSerializer):
    """Commercial Office Serializer"""
    class Meta:  # pylint: disable=C0111
        model = CommercialOffice
        fields = '__all__'
