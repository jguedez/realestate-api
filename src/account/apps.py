"""Application module"""
from django.apps import AppConfig


class AccountConfig(AppConfig):
    """Application configuration"""
    name = 'account'
