"""Tests account app"""

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.response import Response


class AccountTest(APITestCase):
    """Account model tests"""
    fixtures = ['accounts']

    # Login tests
    def post_request(self, reverse_name, data) -> Response:
        """For reduce boilerplate code"""
        url: str = reverse(reverse_name)
        return self.client.post(url, data, format='json')

    def test_succesful_login_account(self):
        """Login with correct credentials"""
        body = {
            'email': 'mbetancourt@negocioselectronicos.biz',
            'password': '123456'
        }
        response = self.post_request('login', body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access', response.data)
        self.assertIn('refresh', response.data)

    def test_empty_login_account(self):
        """Login with correct credentials"""
        response = self.post_request('login', {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fail_login_account(self):
        """Try login with a bad credentials"""
        body = {
            'email': 'mbetancourt@negocioselectronicos.biz',
            'password': '12345678'
        }
        response = self.post_request('login', body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_inactive_login_account(self):
        """Try login with a inactive user"""
        body = {
            'email': 'tcardelli@negocioselectronicos.biz',
            'password': '123456'
        }
        response = self.post_request('login', body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
