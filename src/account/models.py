"""Accounts Model"""

from django.db import models
from django.core.mail import send_mail
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from common.models import LoggingMixin
from account.managers import UserManager


class Account(AbstractBaseUser, PermissionsMixin, LoggingMixin):
    """Account of clients"""
    email = models.EmailField(_('email address'), unique=True)
    is_active = models.BooleanField(_('active'), default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:  # pylint: disable=C0111
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)
