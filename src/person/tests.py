"""Test for module user"""

from model_mommy import mommy
from rest_framework.test import APITestCase

from extensions.tests import (
    CreateTestMixin, ListTestMixin, DestroyTestMixin, ReadOnlyAllowMixin
)

from person.models import Lead, Contact, Agent, Customer
from common.models import CommercialOffice


class CreateLeadTest(APITestCase, CreateTestMixin):
    require_authentication = True
    base_name = 'lead'
    model = Lead
    unique_fields = {'dni', 'email'}
    require_fields = {
        'dni',
        'email',
        'first_name',
        'last_name',
        'address',
        'source'
    }
    response_example = {
        'id',
        'first_name',
        'last_name',
        'dni',
        'phone',
        'address',
        'email',
        'company',
        'source',
        'created_on',
        'updated_on'
    }

    @classmethod
    def setUpTestData(cls):
        cls.body_example = {
            'first_name': 'Pepito',
            'last_name': 'Perez',
            'dni': '2309232',
            'phone': '+584247654154',
            'address': {
                'locality': 'Turumo',
                'street': 'Vereda 4 cruce con vereda 2',
                'postal_code': '542',
                'lat': '423423',
                'long': '123642',
                'city': mommy.make('common.city').id
            },
            'email': 'jguedez25@gmail.com',
            'company': mommy.make('company.CorporateIdentity').id,
            'source': mommy.make('common.source').id
        }


# TODO: add patch test to leads


class ListLeadTest(ListTestMixin, APITestCase):
    require_authentication = True
    base_name = 'lead'
    model = Lead


class DestroyLeadTest(DestroyTestMixin, APITestCase):
    require_authentication = True
    base_name = 'lead'
    model = Lead


class ListContactTest(ListTestMixin, APITestCase):
    require_authentication = True
    base_name = 'contact'
    model = Contact

    @classmethod
    def setUpTestData(cls):
        mommy.make(Agent, 5)
        cls.leads = mommy.make(Lead, 5)
        mommy.make(Customer, 5)

    def test_only_list_agent_customer(self):
        """Test that contact endpoint only return agents or customers not leads
        """
        self.authenticate()
        self.valid_request()
        data = self.get_results(self.response.data)
        leads_id = set(map(lambda x: x.contact_id, self.leads))
        resp_id = set(map(lambda x: x['id'], data))

        contained_ids = leads_id.intersection(resp_id)
        if contained_ids:
            raise AssertionError(
                f'Response contain follows id {contained_ids}'
            )


class ContactReadOnlyTest(ReadOnlyAllowMixin, APITestCase):
    base_name = 'contact'


class ListCommercialOfficeTest(ListTestMixin, APITestCase):
    base_name = 'commercialoffice'
    model = CommercialOffice
    require_authentication = True
