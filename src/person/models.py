"""Person models file"""

from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

from common.models import LoggingMixin

PHONE_VALIDATOR = RegexValidator(
    regex=r'^\+?1?\d{9,15}$',
    message="Phone must be entered in: '+999999999'. Up to 15 digits allowed."
)


class Contact(LoggingMixin):
    """
    `Contact` contain all personal information for `Customer` and `Agents`.
    """
    first_name = models.CharField(_('first name'), max_length=64)
    last_name = models.CharField(_('last name'), max_length=64)
    dni = models.CharField(_('dni'), unique=True, max_length=32)
    couple_married = models.OneToOneField(
        'self', null=True, on_delete=models.CASCADE
    )
    phone = models.CharField(
        validators=[PHONE_VALIDATOR], max_length=17, blank=True
    )
    address = models.OneToOneField('common.Address', on_delete=models.PROTECT)

    class Meta:  # pylint: disable=C0111
        verbose_name = _("contact")
        verbose_name_plural = _("contacts")

    def full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        return self.first_name + ' ' + self.last_name

    def __str__(self):
        """Unicode representation of Customer."""
        return self.full_name()


class Lead(models.Model):
    """
    `Lead` are people who have shown some level of interest in `Product`.

    Leads can remain a lead for 5 minutes or for 5 years. Every individual
    should be entered into the system as a lead.
    """

    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)
    email = models.EmailField(_('email'), unique=True, max_length=320)
    source = models.ForeignKey('common.Source', on_delete=models.PROTECT)
    company = models.ForeignKey(
        'company.CorporateIdentity', on_delete=models.CASCADE, null=True
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _("lead")
        verbose_name_plural = _("leads")


class Agent(models.Model):
    """
    `Agent` are created by administrator and has more permission that `Customer`.
    """

    account = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)
    company = models.ForeignKey(
        'company.Business', null=True, on_delete=models.CASCADE
    )
    office = models.ForeignKey(
        'common.CommercialOffice', on_delete=models.PROTECT
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _("agent")
        verbose_name_plural = _("agents")

    def __str__(self):
        return self.contact.full_name()


class Customer(models.Model):
    """
    `Customer` are created as soon as a `Lead` expresses interest doing business.

    The `Lead` is converted into a customer. Customer are people who are
    attached to `Companies` and are considering going through a transaction
    """

    account = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)
    agents = models.ManyToManyField(
        Agent, through='AssignedAgent', through_fields=('customer', 'agent')
    )
    company = models.ForeignKey(
        'company.Company', null=True, on_delete=models.CASCADE
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _('customer')
        verbose_name_plural = _('customers')

    def __str__(self):
        return self.contact.full_name()


class AssignedAgent(models.Model):
    """
    `Customer` maybe has more that one `Agent`.
    """
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE)
    assigned_on = models.DateTimeField(_('assigned on'), auto_now_add=True)

    class Meta:  # pylint: disable=C0111
        unique_together = ('customer', 'agent')
