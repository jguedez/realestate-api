""" Person views """

from django.db.models import Q
from rest_framework import viewsets, mixins

from person.models import Contact, Lead, Agent
from person.serializers import (
    ContactListSerializer,
    LeadSerializer,
    AgentSerializer
)


class ContactViewSet(
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    """Contact view allow retrieve for anyone"""
    queryset = Contact.objects.filter(
        Q(customer__isnull=False) | Q(agent__isnull=False)).distinct()
    serializer_class = ContactListSerializer


class LeadViewSet(viewsets.ModelViewSet):
    """Lead view allow retrieve for anyone"""
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer


class AgentViewSet(viewsets.ModelViewSet):
    """Agent view allow retrieve for anyone"""
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer
