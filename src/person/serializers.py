"""Serializer file"""

from extensions.serializers import ModelSerializer

from common.serializers import AddressSerializer

from common.models import Address
from person.models import Contact, Lead, Agent


class ContactListSerializer(ModelSerializer):
    address = AddressSerializer()

    class Meta:  # pylint: disable=C0111
        model = Contact
        fields = '__all__'


class ContactSerializer(ModelSerializer):
    """Serializer of contact"""
    address = AddressSerializer()

    class Meta:  # pylint: disable=C0111
        model = Contact
        fields = (
            'first_name',
            'last_name',
            'dni',
            'phone',
            'address',
            'created_on',
            'updated_on'
        )
        read_only_fields = ('created_on', 'updated_on')

    def update(self, instance, validated_data):
        if 'address' in validated_data:
            data = validated_data.pop('address')
            address = instance.address
            address = AddressSerializer(address, data=data, partial=True)
            address.is_valid(raise_exception=True)
            address.save()
        super().update(instance, validated_data)

        return instance


class PersonBaseSerializer(ModelSerializer):
    contact = ContactSerializer()

    def create(self, validated_data):
        contact = validated_data.pop('contact')
        address = Address.objects.create(**contact['address'])
        contact['address'] = address
        contact = Contact.objects.create(**contact)
        validated_data['contact'] = contact
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if 'contact' in validated_data:
            data = validated_data.pop('contact')
            contact = instance.contact
            contact = ContactSerializer(contact, data=data, partial=True)
            contact.is_valid(raise_exception=True)
            contact.save()
        super().update(instance, validated_data)
        return instance


class LeadSerializer(PersonBaseSerializer):
    """Serializer of Lead"""
    class Meta:  # pylint: disable=C0111
        model = Lead
        fields = '__all__'
        flatten = ('contact', )


class AgentSerializer(PersonBaseSerializer):
    """Serializer of agent"""
    class Meta:  # pylint: disable=C0111
        model = Agent
        fields = '__all__'
        flatten = ('contact', )
