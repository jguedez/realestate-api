"""Application module"""
from django.apps import AppConfig


class PersonConfig(AppConfig):
    """Application configuration"""
    name = 'person'
