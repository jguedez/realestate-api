"""All urls of module user"""

from person.views import ContactViewSet, LeadViewSet, AgentViewSet

from extensions.routers import Router

router = Router()

router.register('contacts', ContactViewSet, base_name='contact')
router.register('leads', LeadViewSet, base_name='lead')
router.register('agents', AgentViewSet, base_name='agent')

urlpatterns = router.urls
