"""Product Model"""
from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _
from django.db import models

from person.models import Customer
from common.models import LoggingMixin


class Category(models.Model):
    """Category Module"""
    name = models.CharField(_("names"), unique=True, max_length=64)

    class Meta:  # pylint: disable=C0111
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name


class Currency(models.Model):
    """Currency Module"""
    name = models.CharField(_("names"), unique=True, max_length=45)

    class Meta:  # pylint: disable=C0111
        verbose_name = _("Currency")
        verbose_name_plural = _("Currencies")

    def __str__(self):
        return self.name


class Product(LoggingMixin):
    """Products Module"""
    name = models.CharField(_("names"), max_length=256)
    code = models.CharField(_("codes"), unique=True, max_length=256)
    active = models.BooleanField(_("actives"))
    description = models.TextField(_("descriptions"))
    details = JSONField("details")

    owners = models.ManyToManyField(Customer)
    prices = models.ManyToManyField(
        Currency, through='Price', through_fields=('product', 'currency')
    )

    class Meta:  # pylint: disable=C0111
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return self.name


class Price(models.Model):
    """Pivots Price Books"""
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.DecimalField(_("amounts"), max_digits=10, decimal_places=2)

    class Meta:  # pylint: disable=C0111
        unique_together = ('currency', 'product')
