"""Application module"""
from django.apps import AppConfig


class ProductConfig(AppConfig):
    """Application configuration"""
    name = 'product'
