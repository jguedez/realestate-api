"""realestate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.conf import settings

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

API_INFO = openapi.Info(title='RealEstate API', default_version='v1')
SCHEMA = get_schema_view(
    API_INFO,
    public=bool(settings.DEBUG),
    authentication_classes=(),
    permission_classes=()
)

urlpatterns = [
    path('api/', include([
        path('docs/', SCHEMA.with_ui('redoc'), name='schema'),
        path('auth/', include('account.urls')),
        path('', include('person.urls')),
        path('', include('common.urls')),
        path('', include('company.urls'))
    ]))
]
