# Real Estate API

[![Build Status](https://travis-ci.com/Akhail/RealEstate-API.svg?token=UTMFLaemCZyahvJoDb3o&branch=develop)](https://travis-ci.com/Akhail/RealEstate-API)


## Django commands
* __dcup__: Run containers
* __dce dev manage makemigrations__: Generate migrations from models
* __dce dev manage migrate__: Create migrations on database
* __dce dev manage shell__: Open interactive console ipython
* __dce dev manage makemessages__: Generate i18n messages
* __dce dev manage createsuperuser__: Create superuser

## Steps to launch local server
1. __dcup dev__: Run containers
2. __dce dev manage migrate__: Create migrations on database
3. __dce dev manage createsuperuser__: Create superuser
4. __dcrestart__: Sure reload the django server

## Steps to launch production server
1. Allow port 22, 80 and 443 on firewall
    1. `sudo ufw disable;`
    1. `sudo ufw reset;`
    1. `sudo ufw default deny incoming;`
    1. `sudo ufw default allow outgoing;`
    1. `sudo ufw allow OpenSSH; # is optional only for ssh`
    1. `sudo ufw allow 80/tcp;`
    1. `sudo ufw allow 443/tcp;`
    1. `sudo ufw status; # check port 22, 80, 443 is open`
    1. `sudo ufw enable;`
1. Create production env `cp example.env .env`
1. Create certificate with letsencrypt
1. Run containers __dcup prod__ or __dcup prod db__ use postgres in docker
1. __dce prod manage collectstatic__: Collect statics files
1. __dce prod manage migrate__: Create migrations on database
1. __dce prod manage createsuperuser__: Create superuser *is optional*

### Lets encrypt docker
```bash
docker run -it --rm --name certbot \  
    -v "/etc/letsencrypt:/etc/letsencrypt" \  
    -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \  
    -p 80:80 -p 443:443 \  
    certbot/certbot certonly --standalone \  
    --email email@email.com \  
    --agree-tos -d domain -d www.domain
```

## Troubleshooting
### Unable to connect
* Make sure to execute the command `dcup`
* Check for any error in the code with `dclf api`
### Error 400 bad request
* Set correctly ALLOWED_HOSTS example `ALLOWED_HOSTS=domain;domain2` without "

## Zsh alias
```bash

alias dco='docker-compose -f .docker/docker-compose.yml'

alias dcb='docker-compose -f .docker/docker-compose.yml build'
alias dce='docker-compose -f .docker/docker-compose.yml exec'
alias dcps='docker-compose -f .docker/docker-compose.yml ps'
alias dcrestart='docker-compose -f .docker/docker-compose.yml restart'
alias dcrm='docker-compose -f .docker/docker-compose.yml rm'
alias dcr='docker-compose -f .docker/docker-compose.yml run'
alias dcstop='docker-compose -f .docker/docker-compose.yml stop'
alias dcup='docker-compose -f .docker/docker-compose.yml up -d'
alias dcdn='docker-compose -f .docker/docker-compose.yml down'
alias dcl='docker-compose -f .docker/docker-compose.yml logs'
alias dclf='docker-compose -f .docker/docker-compose.yml logs -f'
alias dcpull='docker-compose -f .docker/docker-compose.yml pull'
alias dcstart='docker-compose -f .docker/docker-compose.yml start'

```